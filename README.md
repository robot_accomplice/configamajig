# configamajig
A flexible XML, YAML, and JSON configuration to/from POJO library that can be leveraged in a wide variety of projects

# Description
Configamajig is the product of my own need to create simply structured, but richly featured configuration files in a reliable and consistent way from project to project. Configuration files can be structured in a way that makes them easily read and understood by people of varying technical ability without compromising support for complex Java objects. Admittedly, Jackson does all of the heavy lifting here, but Configamajig has reduced or obviated the need for a lot of implementation-specific configuration coding in my projects.

# Features
- Apache 2.0 licensed
- Simple and flexible API
- Consistent marshalling and unmarshalling of Java objects to and from XML, JSON, and YAML through Jackson with few to no annotations
- Real-time configuration file monitoring
- Minimal external dependencies

# Authors/Contributors
Jon Machen (@vermicious and @jmachen)

# Maven:
```
<dependency>
    <groupId>com.robotaccomplice</groupId>
    <artifactId>configamajig</artifactId>
    <version>1.2.0</version>
</dependency>
```
# Usage

This Java configuration:

```java
package com.robotaccomplice.configamajig;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Objects;

@JsonIgnoreProperties(value = { "valid" })
@JsonRootName("config")
public class TestConfig implements Configamajig {

    private String portName;
    private int portSpeed;
    private int dataBits;
    private int parity;
    private final boolean valid = true;
    private HashMap<String, String> params;
    private String configFilePath;

    public String getPortName() {
        return this.portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public int getPortSpeed() {
        return portSpeed;
    }

    public void setPortSpeed(int portSpeed) {
        this.portSpeed = portSpeed;
    }

    public int getDataBits() {
        return dataBits;
    }

    public void setDataBits(int dataBits) {
        this.dataBits = dataBits;
    }

    public int getParity() {
        return parity;
    }

    public void setParity(int parity) {
        this.parity = parity;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

    public HashMap<String, String> getParams() {
        return this.params;
    }

    public void setConfigFilePath(String configFilePath) { 
        this.configFilePath = configFilePath;
    }

    public String getConfigFilePath() {
        return this.configFilePath;
    }

    @Override
    public boolean isValid() throws ConfigException {
        return valid;
    }

    @Override
    public boolean equals(Object o) {
        boolean equals = false;

        if (o instanceof TestConfig) {
            TestConfig cs = (TestConfig) o;

            equals = cs.getConfigFilePath().equals(this.getConfigFilePath());
            equals = equals && cs.getPortName().equals(this.getPortName());
            equals = equals && (cs.getPortSpeed() == this.getPortSpeed());
            equals = equals && (cs.getDataBits() == this.getDataBits());
            equals = equals && (cs.getParity() == this.getParity());
            equals = equals && (cs.getParams().equals(this.getParams()));
        }

        return equals;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.portName);
        hash = 79 * hash + this.portSpeed;
        hash = 79 * hash + this.dataBits;
        hash = 79 * hash + this.parity;
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\nconfigFilePath = ").append(this.configFilePath);
        sb.append("\ndataBits  = ").append(this.dataBits);
        sb.append("\nparity    = ").append(this.parity);
        sb.append("\nportName  = ").append(this.portName);
        sb.append("\nportSpeed = ").append(this.portSpeed);

        sb.append("\nparams = ");
        params.keySet().forEach((key) -> {
            sb.append("\n\t").append(key).append(" : ").append(params.get(key));
        });

        return sb.toString();
    }
}

```
Plus this Java invocation 

Yields the following output...

toYamlFile(<path>)
```yaml
---
portName: "COM3"
portSpeed: 38400
dataBits: 8
parity: 0
params:
  param1: "value1"
  param2: "value2"
configFilePath: "/Users/jmachen/code/configamajig/test.yaml"
```

toJsonFile(<path>)
```json
{
  "portName" : "COM3",
  "portSpeed" : 38400,
  "dataBits" : 8,
  "parity" : 0,
  "params" : {
    "param1" : "value1",
    "param2" : "value2"
  },
  "configFilePath" : "/Users/jmachen/code/configamajig/test.json"
}
```
toXmlFile(<path>)
```xml
<config>
  <portName>COM3</portName>
  <portSpeed>38400</portSpeed>
  <dataBits>8</dataBits>
  <parity>0</parity>
  <params>
    <param1>value1</param1>
    <param2>value2</param2>
  </params>
  <configFilePath>/Users/jmachen/code/configamajig/test.xml</configFilePath>
</config>
```

# More Examples
The unit test(s) act as examples of working implementations.  You can review the [code here](https://gitlab.com/robot_accomplice/configamajig/-/tree/master/src/test/java/com/robotaccomplice/configamajig) and example output [files here](https://gitlab.com/robot_accomplice/configamajig/-/tree/master/src/examples)

# Included Dependencies

- <dependency><groupId>com.fasterxml.jackson.dataformat</groupId> <artifactId>jackson-dataformat-yaml</artifactId>  <version>2.17.1</version></dependency>
- <dependency><groupId>com.fasterxml.jackson.dataformat</groupId> <artifactId>jackson-dataformat-xml</artifactId> <version>2.17.1</version></dependency>
- <dependency><groupId>com.fasterxml.jackson.core</groupId> <artifactId>jackson-databind</artifactId> <version>2.17.1</version></dependency>
