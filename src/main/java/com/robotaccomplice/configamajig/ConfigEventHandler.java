/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

import java.io.File;

/**
 * An interface to normalize concrete file modification event handlers
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public interface ConfigEventHandler {

    /**
     * Notifies a concrete implementation of the handler that the referenced watched
     * file was modified
     *
     * @param file the file to watch
     */
    void configModified(File file);
}
