/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

/**
 * A distinct Exception class for Configuration errors
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class ConfigException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Initialize with a message
     *
     * @param message An error message you wish to include with a new instance
     *                of the exception
     */
    public ConfigException(String message) {
        super(message);
    }

    /**
     *
     * @param t A throwable you wish to attach to a new instance of the exception
     */
    public ConfigException(Throwable t) {
        super(t);
    }

    /**
     *
     * @param message An error message you wish to include with a new instance of
     *                the exception
     * @param t       A throwable you wish to attach to a new instance of the
     *                exception
     */
    public ConfigException(String message, Throwable t) {
        super(message, t);
    }
}
