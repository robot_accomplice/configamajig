/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

/**
 * A class which will watch for configuration file changes and notify the
 * specified ConfigEventHandler
 *
 * @param <T> The class of the concrete implementation of the Configamajig
 *            interface
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class ConfigFileWatcher<T extends Configamajig> implements Runnable, AutoCloseable {

    public final static int DEFAULT_INTERVAL = 60000;

    private final File file;
    private final ConfigEventHandler configEventHandler;
    private final int interval;
    private boolean keepWatching = true;

    /**
     * Instantiates a new instance of ConfigFileWatcher
     *
     * @param file               The file to watch
     * @param configEventHandler The event handler that will be notified when a
     *                           change is detected
     * @param interval           The frequency (in milliseconds) to check for file
     *                           changes
     */
    public ConfigFileWatcher(File file, ConfigEventHandler configEventHandler,
            int interval) {
        this.file = file;
        this.configEventHandler = configEventHandler;
        this.interval = interval;
    }

    /**
     * Instantiates a new instance of ConfigFileWatcher using the default
     * interval of 1 minute
     *
     * @param file               The file to watch
     * @param configEventHandler The event handler that will be notified when a
     *                           change is detected
     */
    public ConfigFileWatcher(File file, ConfigEventHandler configEventHandler) {
        this(file, configEventHandler, DEFAULT_INTERVAL);
    }

    /**
     * Run implementation that manages monitoring of the specified file
     */
    @Override
    public void run() {
        Thread.currentThread().setName("ConfigFileWatcher");

        try {
            Path p = Paths.get(this.file.getPath());
            Path parent = p.getParent();

            if (Files.isDirectory(parent, LinkOption.NOFOLLOW_LINKS)) {
                WatchService w = parent.getFileSystem().newWatchService();
                parent.register(w, StandardWatchEventKinds.ENTRY_MODIFY);

                WatchKey wKey = null;
                while (this.keepWatching) {
                    try {
                        wKey = w.poll(this.interval, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException ie) {
                        // interrupted while waiting for poll to return
                        // do nothing and let the loop try the invocation again
                    }

                    if (wKey != null) {
                        for (WatchEvent<?> event : wKey.pollEvents()) {
                            WatchEvent.Kind<?> kind = event.kind();
                            if (kind == OVERFLOW) {
                                continue;
                            }

                            @SuppressWarnings("unchecked") // can only ever be Path
                            WatchEvent<Path> ev = (WatchEvent<Path>) event;
                            Path dir = (Path) wKey.watchable();
                            Path filename = dir.resolve(ev.context());

                            if (filename.toRealPath().equals(p.toRealPath())) {
                                this.configEventHandler.configModified(
                                        filename.toFile());
                            }
                        }

                        if (!wKey.reset()) {
                            break;
                        }
                    }
                }
            } else {
                throw new RuntimeException(parent + " is not a directory!");
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Toggles the keepWatching flag which stops the watch loop
     */
    @Override
    public void close() {
        this.keepWatching = false;
    }
}
