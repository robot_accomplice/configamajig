/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;

/**
 * The interface representing the structure of a concrete implementation of a
 * Configamajig object
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public interface Configamajig {

    /**
     * An abstract method to facilitate implementation specific configuration
     * validation
     *
     * @return Boolean representing whether or not the current configuration passed
     *         implementation specific validation
     * @throws ConfigException if you prefer to throw an exception for invalid
     *                         configs in lieu of returning a boolean
     */
    boolean isValid() throws ConfigException;

    /**
     * Mandatory getter for retrieving the path to the config file as set by the interface
     * @return
     */
    String getConfigFilePath();

    /**
     * Mandatory setter for assigning the path to the config file as determined by the interface
     */
    void setConfigFilePath(String configFilePath);

    /**
     * A utility method for pulling version, vendor, and spec title values from
     * the manifest and returning them as a well formatted string for logging or
     * printing purposes
     *
     * @param c The class for which you want to return manifest details
     * @return String representation of select manifest details (package name,
     *         vendor, and version)
     */
    static String fetchDetailsFromManifest(Class<? extends Configamajig> c) {
        Package objectPackage = c.getPackage();

        String name = objectPackage.getSpecificationTitle();
        String vendor = objectPackage.getSpecificationVendor();
        String specVersion = objectPackage.getSpecificationVersion();

        StringBuilder sb = new StringBuilder();

        sb.append("\n=====================================================");
        sb.append("\n MANIFEST DETAILS: ");
        sb.append("\n=====================================================");
        sb.append("\n Package name           : ").append(name);
        sb.append("\n Package vendor         : ").append(vendor);
        sb.append("\n Package version        : ").append(specVersion);
        sb.append("\n=====================================================");

        return sb.toString();
    }

    /**
     * Will pull the configuration file of the specified class based on a file path
     * specified as system property
     * "config.file"
     *
     * @param <T> The concrete class implementing the Configamajig interface to
     *            which you want to deserialize
     * @param T   The concrete class implementing the Configamajig interface to
     *            which you want to deserialize
     * @return A new instance of the specified class (T)
     * @throws ConfigException thrown if the file property is not found or if we are
     *                         unable to deserialize the file to
     *                         the specified class
     */
    static <T extends Configamajig> T fromXmlFile(Class<? extends Configamajig> T)
            throws ConfigException {
        if (System.getProperty("config.file") == null) {
            throw new ConfigException("config.file property not specified!");
        }

        return fromXmlFile(T, System.getProperty("config.file"));
    }

    /**
     * Will pull the configuration file of the specified class based on a file path
     * specified as system property
     * "config.file"
     *
     * @param <T>      The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param T        The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param filename The name of the existing file which contains a configuration
     *                 matching the object definition in
     *                 class T
     * @return A new instance of the specified class (T)
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to
     *                         the specified class
     */
    static <T extends Configamajig> T fromXmlFile(Class<? extends Configamajig> T, String filename)
            throws ConfigException {
        return fromXmlFile(T, new File(filename));
    }

    /**
     *
     * @param <T>  The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param T    The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param file The file which contains a configuration matching the object
     *             definition in class T
     * @return an instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromXmlFile(Class<? extends Configamajig> T, File file) throws ConfigException {
        return fromFile(T, file, new XmlMapper(new XmlFactory()));
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 config
     * @param config   a concrete object (which implements the Configamajig
     *                 interface) to be serialized
     * @return the file to which we serialized the given config
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         deserialization fails
     */
    static <T extends Configamajig> File toXmlFile(String filename, T config) throws ConfigException {
        config.setConfigFilePath(filename);
        return toFile(filename, config, new XmlMapper(new XmlFactory()));
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 config
     * @return the file to which we serialized the given config
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    default <T extends Configamajig> File toXmlFile(String filename) throws ConfigException {
        this.setConfigFilePath(filename);
        return Configamajig.toXmlFile(filename, this);
    }

    /**
     * Will pull the configuration file of the specified class based on a file path
     * specified as system property
     * "config.file"
     *
     * @param <T> The concrete class implementing the Configamajig interface to
     *            which you want to deserialize
     * @param T   The concrete class implementing the Configamajig interface to
     *            which you want to deserialize
     * @return A new instance of the specified class (T)
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromJsonFile(Class<? extends Configamajig> T)
            throws ConfigException {
        if (System.getProperty("config.file") == null) {
            throw new ConfigException("config.file property not specified!");
        }

        return fromJsonFile(T, System.getProperty("config.file"));
    }

    /**
     * 
     * @param <T>      The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param T        The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param filename The name of the existing file which contains a configuration
     *                 matching the object definition in
     *                 class T
     * @return an instance of the given class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromJsonFile(Class<? extends Configamajig> T, String filename)
            throws ConfigException {
        return fromJsonFile(T, new File(filename));
    }

    /**
     *
     * @param <T>  The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param T    The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param file The existing file which contains a configuration matching the
     *             object definition in class T
     * @return the deserialized instance of the specified class (T)
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromJsonFile(Class<? extends Configamajig> T, File file) throws ConfigException {
        if (file.exists() && file.canRead()) {
            return fromFile(T, file, new ObjectMapper(new JsonFactory()));
        } else {
            throw new ConfigException("Cannot read file : " + file.getAbsolutePath());
        }
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename The name of the file to which we will serialize the given
     *                 configuration object
     * @param config   a concrete object (which implements the Configamajig
     *                 interface) to be serialized
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    static <T extends Configamajig> File toJsonFile(String filename, T config) throws ConfigException {
        config.setConfigFilePath(filename);
        return toFile(filename, config, new ObjectMapper(new JsonFactory()));
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 configuration
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    default <T extends Configamajig> File toJsonFile(String filename) throws ConfigException {
        this.setConfigFilePath(filename);
        return Configamajig.toJsonFile(filename, this);
    }

    /**
     * Will pull the configuration file of the specified class based on a file path
     * specified as system property
     * "config.file"
     *
     * @param <T> The concrete class implementing the Configamajig interface
     * @param T   The concrete class implementing the Configamajig interface
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromYamlFile(Class<? extends Configamajig> T) throws ConfigException {
        if (System.getProperty("config.file") == null) {
            throw new ConfigException("config.file property not specified!");
        }

        return fromYamlFile(T, System.getProperty("config.file"));
    }

    /**
     * 
     * @param <T>      The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param T        The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param filename The name of the existing file which contains a configuration
     *                 matching the object definition in
     *                 class T
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromYamlFile(Class<? extends Configamajig> T, String filename)
            throws ConfigException {
        return fromYamlFile(T, new File(filename));
    }

    /**
     *
     * @param <T>  The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param T    The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param file The existing file which contains a configuration matching the
     *             object definition in
     *             class T
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromYamlFile(Class<? extends Configamajig> T, File file) throws ConfigException {
        if (file.exists() && file.canRead()) {
            return fromFile(T, file, new ObjectMapper(new YAMLFactory()));
        } else {
            throw new ConfigException("Cannot read file : " + file.getAbsolutePath());
        }
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename The name of the file to which we will serialize the given
     *                 config
     * @param config   a concrete object (which implements the Configamajig
     *                 interface) to be serialized
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    static <T extends Configamajig> File toYamlFile(String filename, T config) throws ConfigException {
        config.setConfigFilePath(filename);
        return toFile(filename, config, new ObjectMapper(new YAMLFactory()));
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 config
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    default <T extends Configamajig> File toYamlFile(String filename) throws ConfigException {
        this.setConfigFilePath(filename);
        return Configamajig.toYamlFile(filename, this);
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 config
     * @param mapper   The Jackson ObjectMapper which should be used for
     *                 serialization
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    default <T extends Configamajig> File toFile(String filename, ObjectMapper mapper) throws ConfigException {
        this.setConfigFilePath(filename);
        return Configamajig.toFile(filename, this, mapper);
    }

    /**
     * 
     * @param <T>      A generic representation of the concrete class which
     *                 implements to the Configamajig interface
     * @param filename the name of the file to which we will serialize the given
     *                 config
     * @param config   a concrete object (which implements the Configamajig
     *                 interface) to be serialized
     * @param mapper   The Jackson ObjectMapper which should be used for
     *                 serialization
     * @return the file to which the specified configuration was serialized
     * @throws ConfigException thrown if we are unable to create the file or if
     *                         serialization fails
     */
    static <T extends Configamajig> File toFile(String filename, T config, ObjectMapper mapper) throws ConfigException {
        try {
            File file = new File(filename);
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(file, config);
            return file;
        } catch (IOException ioe) {
            throw new ConfigException("Unable to process configuration file: \""
                    + filename + "\" - " + ioe.getMessage(), ioe);
        }
    }

    /**
     * Attempts to determine the file type based on the file extension
     * 
     * @param <T>      The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param T        The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param filePath A string representing the path to an existing file which
     *                 contains a configuration matching the object definition in
     *                 class T
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromFile(Class<? extends Configamajig> T, String filePath)
            throws ConfigException {
        return fromFile(T, new File(filePath));
    }

    /**
     * Attempts to determine the file type based on the file extension
     * 
     * @param <T>  The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param T    The concrete class implementing the Configamajig interface to
     *             which you want to deserialize
     * @param file An existing file which contains a configuration matching the
     *             object definition in class T
     * @return Class that extends Configamajig, the deserialized instance of the
     *         specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromFile(Class<? extends Configamajig> T, File file) throws ConfigException {
        String filePath = file.getAbsolutePath();

        if (filePath.endsWith(".yaml") || filePath.endsWith(".yml")) {
            return fromYamlFile(T, file);
        } else if (filePath.endsWith(".json")) {
            return Configamajig.fromJsonFile(T, file);
        } else if (filePath.endsWith(".xml")) {
            return Configamajig.fromXmlFile(T, file);
        } else {
            throw new ConfigException("Unsupported file type: " + filePath);
        }
    }

    /**
     * 
     * @param <T>      The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param T        The concrete class implementing the Configamajig interface to
     *                 which you want to deserialize
     * @param filePath A string representing the path to an existing file which
     *                 contains a configuration matching the object definition in
     *                 class T
     * @param mapper   The ObjectMapper to be used for deserialization
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromFile(Class<? extends Configamajig> T, String filePath, ObjectMapper mapper)
            throws ConfigException {
        return fromFile(T, new File(filePath), mapper);
    }

    /**
     *
     * @param <T>    The concrete class implementing the Configamajig interface to
     *               which you want to deserialize
     * @param T      The concrete class implementing the Configamajig interface to
     *               which you want to deserialize
     * @param file   The existing file which contains a configuration matching the
     *               object definition in class T
     * @param mapper The ObjectMapper to be used for deserialization
     * @return the deserialized instance of the specified class
     * @throws ConfigException thrown if the file is not found or if we are unable
     *                         to deserialize the file to the
     *                         specified class
     */
    static <T extends Configamajig> T fromFile(Class<? extends Configamajig> T, File file, ObjectMapper mapper)
            throws ConfigException {
        String filename = file.getAbsolutePath();

        try {
            if (file.exists() && file.canRead()) {
                JavaType type = mapper.getTypeFactory().constructType(T);
                T config = mapper.readValue(file, type);
                if (config.isValid()) {
                    config.setConfigFilePath(file.getAbsolutePath());
                    return config;
                } else {
                    throw new ConfigException("Configuration file is invalid.");
                }
            } else {
                throw new ConfigException("Cannot read file : " + filename);
            }
        } catch (IOException ioe) {
            throw new ConfigException("Unable to process configuration file: \""
                    + filename + "\" - " + ioe.getMessage(), ioe);
        }
    }
}
