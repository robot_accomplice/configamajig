/*
 * Copyright 2022 Jonthan Machen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig.validator;

import java.util.function.Predicate;

/**
 *
 * @author Jonathan Machen
 * @param <FieldType> The generic holding the type of field
 */
public class BasicValidation<FieldType> implements Validation<FieldType> {

    private final Predicate<FieldType> predicate;

    /**
     * 
     * @param predicate
     */
    private BasicValidation(Predicate<FieldType> predicate) {
        this.predicate = predicate;
    }

    /**
     * 
     * @param <FieldType>
     * @param predicate
     * @return
     */
    public static <FieldType> BasicValidation<FieldType> from(Predicate<FieldType> predicate) {
        return new BasicValidation<>(predicate);
    }

    /**
     * 
     * @param field
     * @return
     */
    @Override
    public BasicValidationResult validate(FieldType field) {
        return predicate.test(field) ? BasicValidationResult.ok() : BasicValidationResult.fail();
    }

}
