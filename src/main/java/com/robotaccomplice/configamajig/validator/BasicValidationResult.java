/*
 * Copyright 2022 Jonthan Machen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig.validator;

import java.util.Optional;

/**
 *
 * @author Jonathan Machen
 */
public class BasicValidationResult {

    private final boolean valid;

    /**
     * 
     * @param valid
     */
    private BasicValidationResult(boolean valid) {
        this.valid = valid;
    }

    /**
     * 
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * 
     * @return
     */
    public static BasicValidationResult ok() {
        return new BasicValidationResult(true);
    }

    /**
     * 
     * @return
     */
    public static BasicValidationResult fail() {
        return new BasicValidationResult(false);
    }

    /**
     * 
     * @param field
     * @return
     */
    public Optional<String> getFieldNameIfInvalid(String field) {
        return this.valid ? Optional.empty() : Optional.of(field);
    }
}
