/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig.validator;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 * @param <FieldType> The generic holding the type of field
 */
@FunctionalInterface
public interface Validation<FieldType> {

    public BasicValidationResult validate(FieldType field);

    /**
     * 
     * @param newField
     * @return
     */
    default Validation<FieldType> and(Validation<FieldType> newField) {
        return (field) -> {
            BasicValidationResult validity = this.validate(field);
            return validity.isValid() ? newField.validate(field) : validity;
        };
    }

    default Validation<FieldType> or(Validation<FieldType> newField) {
        return (field) -> {
            BasicValidationResult validity = this.validate(field);
            return validity.isValid() ? validity : newField.validate(field);
        };
    }

    default Validation<FieldType> xor(Validation<FieldType> newField) {
        return (field) -> {
            BasicValidationResult validity = this.validate(field);
            return validity;
        };
    }
}
