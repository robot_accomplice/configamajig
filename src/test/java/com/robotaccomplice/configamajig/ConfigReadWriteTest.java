/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author jmachen
 */
public class ConfigReadWriteTest {

    private final static Logger LOG = Logger.getLogger("TEST");
    private final static boolean CLEANUP = Boolean.parseBoolean(System.getProperty("testCleanup", "true"));

    /**
     * Create the test case
     */
    public ConfigReadWriteTest() {
    }

    /**
     * 
     * @return The current TestConfig
     */
    public TestConfig buildConfig() {
        TestConfig config = new TestConfig();
        config.setPortName("COM3");
        config.setPortSpeed(38400);
        config.setDataBits(8);
        config.setParity(0);
        HashMap<String, String> myMap = new HashMap<>();
        myMap.put("param1", "value1");
        myMap.put("param2", "value2");
        config.setParams(myMap);

        return config;
    }

    /**
     * Read/write test for JSON files
     */
    @Test
    public void testJsonConfigWriteAndRead() {
        System.out.println("\n\n*** JSON Read/Write config file test");
        File toFile = new File("test.json");
        if (toFile.exists())
            toFile.delete();

        try {
            toFile.createNewFile();
            TestConfig config = buildConfig();

            config.toJsonFile(toFile.getAbsolutePath());

            TestConfig fromJsonFile = Configamajig.fromJsonFile(TestConfig.class, toFile.getAbsolutePath());

            System.out.println("*** Config: ");
            System.out.println(config);

            System.out.println("*** from JSON file: ");
            System.out.println(fromJsonFile);

            Assertions.assertEquals(config, fromJsonFile);
            System.out.println("config contents are equivalent!");
        } catch (ConfigException | IOException e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            if (CLEANUP && toFile.exists())
                toFile.delete();
        }
    }

    /**
     * Read/write test for YAML files
     */
    @Test
    public void testYamlConfigWriteAndRead() {
        System.out.println("\n\n*** Yaml Read/Write config file test");
        File toFile = new File("test.yml");
        if (toFile.exists())
            toFile.delete();

        try {
            toFile.createNewFile();
            TestConfig config = buildConfig();

            config.toYamlFile(toFile.getAbsolutePath());

            TestConfig fromFile = Configamajig.fromYamlFile(TestConfig.class, toFile.getAbsolutePath());

            System.out.println("*** config: ");
            System.out.println(config);

            System.out.println("*** yaml file: ");
            System.out.println(fromFile);

            Assertions.assertEquals(config, fromFile);
            System.out.println("config contents are equivalent!");
        } catch (ConfigException | IOException e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            if (CLEANUP && toFile.exists())
                toFile.delete();
        }
    }

    /**
     * Read/write test for XML files
     */
    @Test
    public void testXmlConfigWriteAndRead() {
        System.out.println("\n\n*** XML Read/Write config file test");
        File toFile = new File("test.xml");
        if (toFile.exists())
            toFile.delete();

        try {
            toFile.createNewFile();
            TestConfig config = buildConfig();

            config.toXmlFile(toFile.getAbsolutePath());

            TestConfig fromFile = Configamajig.fromXmlFile(TestConfig.class, toFile.getAbsolutePath());

            System.out.println("*** to file: ");
            System.out.println(config);

            System.out.println("*** from file: ");
            System.out.println(fromFile);

            Assertions.assertEquals(config, fromFile);
            System.out.println("config contents are equivalent!");
        } catch (ConfigException | IOException e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            if (CLEANUP && toFile.exists())
                toFile.delete();
        }
    }

    /**
     * Tests that the fromFile method properly detects the file extension
     */
    @Test
    public void testFromFileExtensionDetection() {
        System.out.println("\n\n*** File extension detection");
        File xmlFile = new File("unit_test.xml");
        if (xmlFile.exists())
            xmlFile.delete();
        File ymlFile = new File("unit_test.yml");
        if (ymlFile.exists())
            ymlFile.delete();
        File yamlFile = new File("unit_test.yaml");
        if (yamlFile.exists())
            yamlFile.delete();
        File jsonFile = new File("unit_test.json");
        if (jsonFile.exists())
            jsonFile.delete();

        try {
            TestConfig config = buildConfig();

            config.toJsonFile(jsonFile.getAbsolutePath());
            config.toXmlFile(xmlFile.getAbsolutePath());
            config.toYamlFile(ymlFile.getAbsolutePath());
            config.toYamlFile(yamlFile.getAbsolutePath());

            TestConfig fromFile = Configamajig.fromFile(TestConfig.class, xmlFile.getAbsolutePath());
            Assertions.assertNotNull(fromFile);

            fromFile = Configamajig.fromFile(TestConfig.class, ymlFile.getAbsolutePath());
            Assertions.assertNotNull(fromFile);

            fromFile = Configamajig.fromFile(TestConfig.class, yamlFile.getAbsolutePath());
            Assertions.assertNotNull(fromFile);

            fromFile = Configamajig.fromFile(TestConfig.class, jsonFile.getAbsolutePath());
            Assertions.assertNotNull(fromFile);
        } catch (ConfigException e) {
            LOG.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            if (CLEANUP && xmlFile.exists())
                xmlFile.delete();
            if (CLEANUP && ymlFile.exists())
                ymlFile.delete();
            if (CLEANUP && yamlFile.exists())
                yamlFile.delete();
            if (CLEANUP && jsonFile.exists())
                jsonFile.delete();
        }
    }
}
