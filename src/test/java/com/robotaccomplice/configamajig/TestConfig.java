/*
 * Copyright 2016-2022 Jonathan Machen <jon.machen@robotaccomplice.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robotaccomplice.configamajig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Objects;

/**
 *
 * @author jmachen
 */
@JsonIgnoreProperties(value = { "valid" })
@JsonRootName("config")
public class TestConfig implements Configamajig {

    private String portName;
    private int portSpeed;
    private int dataBits;
    private int parity;
    private HashMap<String, String> params;
    private String configFilePath;

    /**
     *
     * @return
     */
    public String getPortName() {
        return portName;
    }

    /**
     *
     * @param portName
     */
    public void setPortName(String portName) {
        this.portName = portName;
    }

    /**
     *
     * @return
     */
    public int getPortSpeed() {
        return portSpeed;
    }

    /**
     *
     * @param portSpeed
     */
    public void setPortSpeed(int portSpeed) {
        this.portSpeed = portSpeed;
    }

    /**
     *
     * @return
     */
    public int getDataBits() {
        return dataBits;
    }

    /**
     *
     * @param dataBits
     */
    public void setDataBits(int dataBits) {
        this.dataBits = dataBits;
    }

    /**
     *
     * @return
     */
    public int getParity() {
        return parity;
    }

    /**
     *
     * @param parity
     */
    public void setParity(int parity) {
        this.parity = parity;
    }

    /**
     *
     * @param params
     */
    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

    /**
     *
     * @return
     */
    public HashMap<String, String> getParams() {
        return this.params;
    }

    /**
     *
     * @param configFilePath
     */
    public void setConfigFilePath(String configFilePath) { this.configFilePath = configFilePath; }

    /**
     *
     * @return
     */
    public String getConfigFilePath() { return this.configFilePath; }

    /**
     *
     * @return @throws ConfigException
     */
    @Override
    public boolean isValid() throws ConfigException {
        return true;
    }

    /**
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        boolean equals = false;

        if (o instanceof TestConfig cs) {
            equals = cs.getConfigFilePath().equals(this.getConfigFilePath());
            equals = equals && cs.getPortName().equals(this.getPortName());
            equals = equals && (cs.getPortSpeed() == this.getPortSpeed());
            equals = equals && (cs.getDataBits() == this.getDataBits());
            equals = equals && (cs.getParity() == this.getParity());
            equals = equals && (cs.getParams().equals(this.getParams()));
        }

        return equals;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.portName);
        hash = 79 * hash + this.portSpeed;
        hash = 79 * hash + this.dataBits;
        hash = 79 * hash + this.parity;
        return hash;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\nconfigFilePath = ").append(this.configFilePath);
        sb.append("\ndataBits  = ").append(this.dataBits);
        sb.append("\nparity    = ").append(this.parity);
        sb.append("\nportName  = ").append(this.portName);
        sb.append("\nportSpeed = ").append(this.portSpeed);

        sb.append("\nparams = ");
        params.keySet().forEach((key) -> sb.append("\n\t").append(key).append(" : ").append(params.get(key)));

        return sb.toString();
    }
}
